import numpy as np

def random_cov(nn, mean=0):
    qq = np.random.randn(nn, nn)
    cov = np.dot(np.dot(qq.T, np.diag(np.abs(mean + np.random.randn(nn)))), qq)
    return cov

def xxcholupdate(LL, vv, alpha=1, beta=1):
    """
    Return the updated trianguler cholesky factor LL' of
    alpha * SS + beta * np.outer(vv, vv.T)
    where SS = np.dot(LL, LL.T)
    A More Efficient Rank-one Covariance Matrix Update for Evolution Strategies
    Oswin Krause, Christian Igel 2015
    https://christian-igel.github.io/paper/AMERCMAUfES.pdf
    """
    assert LL.shape[0] == LL.shape[1] == len(vv)

    omega = vv
    b = 1
    LLp = np.zeros(LL.shape)
    for jj in range(len(vv)):
        LLp[jj, jj] = np.sqrt(alpha * LL[jj,jj] * LL[jj,jj] + (beta / b) * omega[jj] * omega[jj])
        gamma = alpha * LL[jj,jj] * LL[jj,jj] * b + beta * omega[jj] * omega[jj]
        for kk in range(jj+1, len(vv)):
            omega[kk] = omega[kk] - (omega[jj] / LL[jj, jj]) * np.sqrt(alpha) * LL[kk, jj]
            LLp[kk, jj] = np.sqrt(alpha) * LLp[jj, jj] * LL[kk, jj] / LL[jj, jj] + LLp[jj, jj] * beta * omega[jj] * omega[kk] / gamma
        b = b + beta * omega[jj] * omega[jj] / (alpha * LL[jj, jj] * LL[jj, jj])
    return LLp

def cholupdate(L, x, sign=1):
    assert L.shape[0] == L.shape[1] == len(x)
    assert sign == 1 or sign == -1, 'only cholupdate / downdate'
    for jj in range(len(x)):
        rr = np.sqrt((L[jj, jj]**2) + sign*(x[jj]**2))
        cc = rr/L[jj, jj]
        ss = x[jj]/L[jj, jj]
        L[jj, jj] = rr
        for ii in range(jj+1, len(x)):
            L[ii, jj] = (L[ii, jj] + sign*ss*x[ii])/cc;
            x[ii] = cc*x[ii] - ss*L[ii, jj];
    return L
def mctrial():
    nn = int(np.round(np.random.uniform(10, 100)))

    AA = random_cov(nn)
    vv = np.random.randn(nn)

    alpha = 1
    beta = 1

    LL = np.linalg.cholesky(AA)
    UU = alpha * AA + beta * np.outer(vv, vv)
    LLu = np.linalg.cholesky(UU)

    LLp = cholupdate(LL, vv)
    np.testing.assert_almost_equal(LLp,  LLu, 10)

ii = 0
while True:
    mctrial()
    ii += 1
    print(ii)

#vv = np.array([1, 2, 3, 4, 5])
#AA = np.eye(5) * vv
#LL = np.linalg.cholesky(AA)
#vv_vvt = np.outer(vv, vv)
#UU = AA + vv_vvt
#LLp = cholupdate(LL, vv)
#print(AA)
#print(np.linalg.cholesky(AA))
#print(np.linalg.cholesky(UU))
#print(LLp)
#print(UU)
#print(np.dot(LLp, LLp.T))
#print(LLp - np.linalg.cholesky(UU))
##np.random.seed(1)
##AA = random_cov(3)
##LL = np.linalg.cholesky(AA)
##vv = np.random.randn(3)
##UU = AA + np.outer(vv, vv.T)
##LLp = cholupdate(LL, vv, beta=1, alpha=0)
##print(np.outer(vv.T, vv))
##print(np.linalg.cholesky(np.outer(vv.T, vv)))
###print(np.linalg.cholesky(UU))
##print(LL)
##print(LLp)
###print(UU)
###print(np.dot(LLp, LLp.T))
#
